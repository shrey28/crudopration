import React, { useEffect, useState } from 'react'
import { Link, useParams } from "react-router-dom";

const ViewState = () => {
 

  const [getuserdata, setUserdata] = useState([]);
  console.log(getuserdata);

  const { id } = useParams("");
  console.log(id);

  const getdata = async () => {
    const res = await fetch(`/getuser/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    console.log(data);

    if (res.status === 422 || !data) {
      console.log("Error");
    } else {
      console.log("Data Added");
      setUserdata(data);
    }
  };

  useEffect(() => {
    getdata();
}, [])
  return (
    <div className="container mt-5">
      <Link className="btn btn-primary" to="/">
        Home
      </Link>
      <div className="row mt-5">
        <div className="col-md-6">
          <ul class="list-group">
            <li class="list-group-item active" aria-current="true">
              State Datails
            </li>
            <li class="list-group-item">ID :- {getuserdata._id}</li>
            <li class="list-group-item">India :- {getuserdata.name}</li>
            <li class="list-group-item">State :- {getuserdata.State}</li>
            <li class="list-group-item">Disrict :- {getuserdata.Disrict}</li>
            <li class="list-group-item">Block :- {getuserdata.Block}</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ViewState;
