import React, { useState, useEffect } from "react";

import { Link, useParams, useNavigate } from "react-router-dom";
const EditSate = () => {
  // const [getuserdata, setUserdata] = useState([]);
  // console.log(getuserdata);
  // const {updata, setUPdata} = useContext(updatedata)
  const history = useNavigate();
  const [inputVal, setINP] = useState({
    name: "",
    State: "",
    Disrict: "",
    Block: "",
  });
  const setdata = (e) => {
    let names, value;
    names = e.target.name
    value = e.target.value
    setINP({...inputVal,[names]:value})
  };
  const { id } = useParams("");
  console.log(id);

  const getdata = async (e) => {
    const res = await fetch(`/getuser/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    console.log(data);

    if (res.status === 422 || !data) {
      console.log("error ");
    } else {
      console.log("get data");
      setINP(data);
    }
  };

  useEffect(() => {
    getdata();
  }, []);


  const updateUser = async (e) => {
    e.preventDefault();
    const { name, State, Disrict, Block } = inputVal;
    const res = await fetch(`/updateuser/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        State,
        Disrict,
        Block,
      }),
    });
 
  const data = await res.json();
  console.log(data);

  if (res.status === 422 || !data) {
    alert("fill the data");

  } else {
    alert("data added");
        history('/')
      // setINP(data)
      // console.log("get data");

  }
}

  return (
    <div className="container mt-5">
      <form className="mx-auto  w-50 shadow p-5">
        <Link className="btn btn-primary" to="/">
          Home
        </Link>
        <h1 className="mt-5">Fill-Up Edit Details</h1>
        <div class="mb-3">
          <label htmlFor="exampleInputEmail1" class="form-label">
          Country
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            name="name"
            onChange={setdata}
            value={inputVal.name}
            aria-describedby="emailHelp"
          />
        </div>
        <div class="mb-3">
          <label htmlFor="exampleInputEmail1" class="form-label">
            State
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            name="State"
            onChange={setdata}
            value={inputVal.State}
            aria-describedby="emailHelp"
          />
        </div>
        <div class="mb-3">
          <label htmlFor="exampleInputEmail1" class="form-label">
            Disrict
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            name="Disrict"
            onChange={setdata}
            value={inputVal.Disrict}
            aria-describedby="emailHelp"
          />
        </div>
        <div class="mb-3">
          <label htmlFor="exampleInputEmail1" class="form-label">
            Block
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            name="Block"
            onChange={setdata}
            value={inputVal.Block}
            aria-describedby="emailHelp"
          />
        </div>
        <button type="submit" className="btn btn-primary" onClick={updateUser}>
          Add
        </button>
      </form>
    </div>
  );
};

export default EditSate;
