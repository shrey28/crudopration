import React, { useState, useEffect } from "react";
import DataTable from "react-data-table-component";
import { Link, NavLink,useNavigate } from "react-router-dom";

const AllStat = () => {
  const [getuserdata, setUserdata] = useState([]);
  const [userid, setUserid] = useState("");
  const [searchFilter, setSearchFilter] = useState("");
  const history = useNavigate();

  const inputChanged = (event) => {
    setUserid(event.target.value);
  };
  console.log(getuserdata);

  const getdata = async (e) => {
    const res = await fetch("/getdata", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await res.json();
    console.log(data);

    if (res.status === 422 || !data) {
      alert("Error");
    } else {
      setUserdata(data);
    }
  };

  useEffect(() => {
    getdata();
  }, []);

  useEffect(() => {
    setSearchFilter(
      getuserdata.filter((result) =>
        result.State.toLowerCase().includes(userid.toLowerCase())
      )
    );
  }, [userid, getuserdata]);

  const columns = [
    {
      name: "Counrty Name",
      selector: (row) => row.name,
      sortable: true,
    },
    {
      name: "Counrty State",
      selector: (row) => row.State,
    },
    {
      name: " State Id",
      selector: (row) => row._id,
    },

    {
      name: "Disrict",
      selector: (row) => row.Disrict,
    },
    {
      name: "Block",
      selector: (row) => row.Block,
    },
    {
      name: "Action",
      cell: (row) => (
        <NavLink to={`view/${row._id}`}>
          <button className="btn btn-secondary">View</button>
        </NavLink>
      ),
    },
    {
      name: "Action",
      cell: (row) => (
        <NavLink to={`edit/${row._id}`}>
          <button className="btn btn-primary">Edit</button>
        </NavLink>
      ),
    },
    {
      name: "Action",
      cell: (row) => (
        <button className="btn btn-danger" onClick={() => deleteUser(row._id)}>
          Delete
        </button>
      ),
    },
  ];

  const deleteUser = async (id) => {
    const res = await fetch(`/deleteuser/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const detedata = await res.json();
    alert(detedata);
    if (res.status === 422 || !detedata) {
     
    } else {
     
      getdata();
      history('/')
    }
  };
  return (
    <div className="container mt-5">
      <Link type="button" class="btn btn-primary me-3" to="/addStat">
        Create Data
      </Link>
      <DataTable
        subHeader
        subHeaderComponent={
          <input
            placeholder="Search here"
            name="search"
            value={userid}
            onChange={inputChanged}
          />
        }
        highlightOnHover
        selectableRowsHighlight
        fixedHeaderScrollHeight="600px"
        fixedHeader
        columns={columns}
        data={searchFilter}
        pagination
      />
    </div>
  );
};

//     <div className="container mt-5">
//       <div className="mt-3">
//         <Link type="button" class="btn btn-primary me-3" to="/addStat">
//           Add Detail
//         </Link>
//       </div>

//       <div>
//         <table class="table mt-3">
//           <thead>
//             <tr className="bg-warning text-dark">
//               <th scope="col">#</th>
//               <th>ID</th>
//               <th scope="col">Country</th>
//               <th scope="col">State</th>
//               <th scope="col">Disrict</th>
//               <th>Block</th>
//               <th>Action</th>
//             </tr>
//           </thead>
//           <tbody>
//             {getuserdata.map((element, id) => {
//               return (
//                 <>
//                   <tr>
//                     <th scope="row">{id + 1}</th>
//                     <td>{element._id}</td>
//                     <td>{element.name}</td>
//                     <td>{element.State}</td>
//                     <td>{element.Disrict}</td>
//                     <td>{element.Block}</td>
//                     <td>
//                       <NavLink to={`view/${element._id}`}><button type="button" class="btn btn-info me-3">
//                         View
//                       </button></NavLink>
//                       <NavLink to={}>  <button type="button" class="btn btn-danger me-3">
//                         Delete
//                       </button></NavLink>
//                       <NavLink to={`edit/${element._id}`><button type="button" class="btn btn-warning me-3">
//                         Edit
//                       </button></NavLink>
//                     </td>
//                   </tr>
//                 </>
//               );
//             })}
//           </tbody>
//         </table>
//       </div>
//     </div>
//   );
// };
//)

export default AllStat;
