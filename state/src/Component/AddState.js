import React, { useState } from "react";
import { Link,useNavigate } from "react-router-dom";


const AddState = () => {
  const history = useNavigate();
  const [inputVal, setInputVal] = useState({
    name: "",
    State: "",
    Disrict: "",
    Block: "",
  });
  const handalinput = (e) => {
  let names, value;
   names = e.target.name
   value = e.target.value
   setInputVal({...inputVal,[names]:value})
  };

  const PostData = async (e) => {
    e.preventDefault();
    const { name, State, Disrict, Block } = inputVal;
    const res = await fetch('/addStat', {
      method:"POST",
    headers:{
      "Content-Type":"application/json"
    },
      body: JSON.stringify({
        name,
        State,
        Disrict,
        Block,
      }),
    });

    const data = await res.json();
    console.log(data);

    if (res.status === 422 || !data) {
      alert("Error");
    } else {

      setInputVal(data);
      history('/')
    }
  };

  return (
    <div className="container mt-5">
      <form method="POST" className="mx-auto  w-50 shadow p-5">
        <Link className="btn btn-primary" to="/">
          Home
        </Link>
        <h1 className="mt-5">Fill-Up Details</h1>
        <div class="mb-3">
          <label htmlFor="exampleInputEmail1" class="form-label">
          Country
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            name="name"
            onChange={handalinput}
            value={inputVal.name}
          />
        </div>
        <div class="mb-3">
          <label htmlFor="exampleInputEmail1" class="form-label">
            State
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            name="State"
            onChange={handalinput}
            value={inputVal.State}
          />
        </div>
        <div class="mb-3">
          <label htmlFor="exampleInputEmail1" class="form-label">
            Disrict
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            name="Disrict"
            onChange={handalinput}
            value={inputVal.Disrict}
          />
        </div>
        <div class="mb-3">
          <label htmlFor="exampleInputEmail1" class="form-label">
            Block
          </label>
          <input
            type="text"
            class="form-control"
            id="exampleInputEmail1"
            name="Block"
            onChange={handalinput}
            value={inputVal.Block}
          />
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={PostData}
        >
          Add
        </button>
      </form>
    </div>
  );
};

export default AddState;
