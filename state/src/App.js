import "./App.css";
import AddState from "./Component/AddState";
import AllStat from "./Component/AllStat";
import EditSate from "./Component/EditState";
import Navbar from "./Component/Navbar";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ViewState from "./Component/ViewState";

function App() {
  return (
   <BrowserRouter>
   <Navbar/>
   <Routes>
    <Route path="/" element={<AllStat/>}/>
    <Route path="/addStat" element={<AddState/>}/>
    <Route path="/edit/:id" element={<EditSate/>}/>
    <Route path="/view/:id" element={<ViewState/>}/>
   </Routes>
   </BrowserRouter>
  );
}

export default App;
