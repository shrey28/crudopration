const express = require("express");
const router = express.Router();
const states = require("../model/StateSechema");

//Post Api
router.post("/addStat", async (req, res) => {
  console.log(req.body);

  const { name, State, Disrict, Block } = req.body;
  if (!name || !State || !Disrict || !Block) {
    res.status(404).json("Please fill the Data");
  }
  try {
    const prestate = await states.findOne({ Block: Block });
    if (prestate) {
      res.status(404).json("This Block Already Present");
    } else {
      const addState = new states({ name, State, Disrict, Block });
      await addState.save();
      res.status(201).json(addState);
    }
  } catch (err) {
    res.status(404).json(err);
  }
});

//get userdata
router.get("/getdata",async(req,res)=>{
  try {
      const userdata = await states.find();
      res.status(201).json(userdata)
      console.log(userdata);
  } catch (error) {
      res.status(422).json(error);
  }
})


// update user data

router.patch("/updateuser/:id",async(req,res)=>{
  try {
      const {id} = req.params;

      const updateduser = await states.findByIdAndUpdate(id,req.body,{
          new:true
      });

      console.log(updateduser);
      res.status(201).json(updateduser);

  } catch (error) {
      res.status(422).json(error);
  }
})


// get individual user

router.get("/getuser/:id",async(req,res)=>{
  try {
      console.log(req.params);
      const {id} = req.params;

      const userindividual = await states.findById({_id:id});
      console.log(userindividual);
      res.status(201).json(userindividual)

  } catch (error) {
      res.status(422).json(error);
  }
})


//delete
router.delete("/deleteuser/:id",async(req,res)=>{
  try {
      const {id} = req.params;

      const deletuser = await states.findByIdAndDelete({_id:id})
      console.log(deletuser);
      res.status(201).json(deletuser);

  } catch (error) {
      res.status(422).json(error);
  }
})


module.exports = router;
