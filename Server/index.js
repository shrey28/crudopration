const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv")
var cors = require("cors");
const app = express();

//add schema

const State = require("./model/StateSechema")

//add router

const  router = require("./routers/router")


dotenv.config()
app.use(cors());
app.use(express.json())
app.use(router)



const Database = process.env.DataBaseHide
  
const connectionParams = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
mongoose
  .connect(Database, connectionParams)
  .then(() => {
    console.log(`Mongoose is Connected`);
  })
  .catch((err) => {
    console.log(`Mongoose is Not Connected please check Error`);
  });

app.listen(6000);
