const mongoose = require("mongoose");
const StateSechema = new mongoose.Schema({
  name: {
    type: String,
    require: true,
  },
  State: {
    type: String,
    require: true,
  },
  Disrict: {
    type: String,
    require: true,
  },
  Block: {
    type: String,
    require: true,
  },
});

const State = new mongoose.model("State", StateSechema);

module.exports = State;
